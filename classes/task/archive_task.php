<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Legacy log archive log reader cleanup task.
 *
 * @package    logstore_archivelegacy
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2015 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace logstore_archivelegacy\task;

/**
 * Legacy log archive log reader cleanup task.
 * It moves log entries from the logstore_legacy database table into a
 * table on another database when they are older than a configurable time
 * period.
 *
 * @package    logstore_archivelegacy
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2015 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class archive_task extends \core\task\scheduled_task {
    use \logstore_archivelegacy\helper\config,
        \logstore_archivelegacy\helper\externaldb;

    /** @var core\log\sql_select_reader The legacy logstore. */
    protected $legacylogstore = null;

    /**
     * Get a descriptive name for this task (shown to admins).
     *
     * @return string
     */
    public function get_name() {
        return get_string('taskarchive', 'logstore_archivelegacy');
    }

    /**
     * Do the job.
     * Throw exceptions on errors (the job will be retried).
     * @global \moodle_database $DB
     * @return void
     */
    public function execute() {
        global $DB;
        if (!$this->init()) {
            mtrace("Database not setup.");
            return;
        }
        $logtable = $this->get_config('dbtable');

        $loglifetime = (int)$this->get_config('archiveafter');
        if (empty($loglifetime) || $loglifetime < 0) {
            return;
        }

        $loglifetime = time() - $loglifetime;
        $start = time();

        $records = $this->get_records_to_archive($loglifetime);
        $count = 0; // The count of events archived.
        while (!empty($records)) {
            $count += count($records);
            $deleteids = array_keys($records);
            $this->extdb->insert_records($logtable, $records);
            // Delete the records we copied by id.
            list($select, $params) = $DB->get_in_or_equal($deleteids, SQL_PARAMS_NAMED, 'id');
            $DB->delete_records_select('log', "id $select", $params);
            if (time() > $start + 300) {
                // Do not churn on log deletion for too long each run.
                break;
            }
            // Get more records.
            $records = $this->get_records_to_archive($loglifetime);
        }
        mtrace(" Archived $count log records from legacy store to the legacy log archive.");
    }

    /**
     * Get a set of up to 1000 records that can be archived.
     *
     * @global \moodle_database $DB The Moodle database connection object.
     * @param int $archivetime
     * @return array
     */
    protected function get_records_to_archive($archivetime) {
        global $DB;
        $table = 'log';
        $select = "time < :archivetime";
        $params = array('archivetime' => $archivetime);
        return $DB->get_records_select($table, $select, $params, 'id', '*', 0, 1000);
    }
}
