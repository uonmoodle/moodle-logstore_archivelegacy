<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Legacy log archive store settings.
 *
 * @package    logstore_archivelegacy
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2015 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

if ($hassiteconfig) {
    $testurl = new moodle_url('/admin/tool/log/store/archivelegacy/test_settings.php', array('sesskey' => sesskey()));
    $test = new admin_externalpage('logstorearchivelegacytestsettings', get_string('testsettings', 'logstore_archivelegacy'),
        $testurl, 'moodle/site:config', true);
    $ADMIN->add('logging', $test);

    $drivers = \logstore_archivelegacy\helper::get_drivers();
    // Database settings.
    $link = html_writer::link($testurl, get_string('testsettings', 'logstore_archivelegacy'), array('target' => '_blank'));
    $settings->add(new admin_setting_heading('dbsettings', get_string('databasesettings', 'logstore_archivelegacy'),
        get_string('databasesettings_help', 'logstore_archivelegacy', $link)));
    $settings->add(new admin_setting_configselect('logstore_archivelegacy/dbdriver', get_string('databasetypehead', 'install'), '',
        '', $drivers));

    $settings->add(new admin_setting_configtext('logstore_archivelegacy/dbhost', get_string('databasehost', 'install'), '', ''));
    $settings->add(new admin_setting_configtext('logstore_archivelegacy/dbuser', get_string('databaseuser', 'install'), '', ''));
    $settings->add(
        new admin_setting_configpasswordunmask(
            'logstore_archivelegacy/dbpass',
            get_string('databasepass', 'install'),
            '',
            ''
        )
    );
    $settings->add(new admin_setting_configtext('logstore_archivelegacy/dbname', get_string('databasename', 'install'), '', ''));
    $settings->add(
        new admin_setting_configtext(
            'logstore_archivelegacy/dbtable',
            get_string('databasetable', 'logstore_archivelegacy'),
            get_string('databasetable_help', 'logstore_archivelegacy'),
            ''
        )
    );

    $settings->add(new admin_setting_configcheckbox('logstore_archivelegacy/dbpersist', get_string('databasepersist',
        'logstore_archivelegacy'), '', '0'));
    $settings->add(new admin_setting_configtext('logstore_archivelegacy/dbsocket', get_string('databasesocket', 'install'), '',
        ''));
    $settings->add(new admin_setting_configtext('logstore_archivelegacy/dbport', get_string('databaseport', 'install'), '', ''));
    $settings->add(new admin_setting_configtext('logstore_archivelegacy/dbschema', get_string('databaseschema',
        'logstore_archivelegacy'), '', ''));
    $settings->add(new admin_setting_configtext('logstore_archivelegacy/dbcollation', get_string('databasecollation',
        'logstore_archivelegacy'), '', ''));

    // Time before logs are archived, values are 1 day less than the delete settings in the standard logstore.
    $settings->add(new admin_setting_configduration(
            'logstore_archivelegacy/archiveafter',
            get_string('archiveafter', 'logstore_archivelegacy'),
            get_string('configarchiveafter', 'logstore_archivelegacy'),
            0,
            DAYSECS
    ));

    // Time to store archived logs for.
    $settings->add(new admin_setting_configduration(
            'logstore_archivelegacy/archivelifetime',
            get_string('archivelifetime', 'logstore_archivelegacy'),
            get_string('configarchivelifetime', 'logstore_archivelegacy'),
            0,
            DAYSECS
    ));
}
