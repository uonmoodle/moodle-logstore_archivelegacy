<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace logstore_archivelegacy\log;

use context_course;

/**
 * Legacy log archive store tests.
 *
 * Tests the log store class of logstore_archivelegacy.
 *
 * @package    logstore_archivelegacy
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2015 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @group logstore_archivelegacy
 * @group uon
 */
class store_test extends \advanced_testcase {
    use \logstore_archivelegacy\local\testing\local_table;

    /**
     * Tests that the archive task moves events correctly.
     *
     * @covers \logstore_archivelegacy\task\archive_task::execute
     * @group logstore_archivelegacy
     * @group uon
     */
    public function test_archive_task() {
        global $DB;
        $this->resetAfterTest();
        $expectedoutput = '';
        // Create some records spread over various days.
        $ctx = context_course::instance(1);
        $record = (object) array(
            'edulevel' => 0,
            'contextid' => $ctx->id,
            'contextlevel' => $ctx->contextlevel,
            'contextinstanceid' => $ctx->instanceid,
            'userid' => 1,
            'time' => time(),
        );

        $DB->insert_record('log', $record);
        $record->time -= 3600 * 24 * 30;
        $DB->insert_record('log', $record);
        $record->time -= 3600 * 24 * 30;
        $DB->insert_record('log', $record);
        $record->time -= 3600 * 24 * 30;
        $DB->insert_record('log', $record);
        $this->assertEquals(4, $DB->count_records('log'));
        $this->assertEquals(0, $DB->count_records(self::$tablename));

        // Remove all logs before "today".
        set_config('archiveafter', DAYSECS, 'logstore_archivelegacy');
        // Force a reset of the log manager otherwise this test may fail.
        get_log_manager(true);

        $expectedoutput .= " Archived 3 log records from legacy store to the legacy log archive.\n";
        $clean = new \logstore_archivelegacy\task\archive_task();
        $clean->execute();

        $this->assertEquals(1, $DB->count_records('log'));
        $this->assertEquals(3, $DB->count_records(self::$tablename));
        $this->expectOutputString($expectedoutput);
    }

    /**
     * Test that the standard archive log cleanup works correctly.
     *
     * @covers \logstore_archivelegacy\task\cleanup_task::execute
     * @group logstore_archivelegacy
     * @group uon
     */
    public function test_cleanup_task() {
        global $DB;
        $this->resetAfterTest();
        // Create some records spread over various days; test multiple iterations in cleanup.
        $ctx = context_course::instance(1);
        $record = (object) array(
            'edulevel' => 0,
            'contextid' => $ctx->id,
            'contextlevel' => $ctx->contextlevel,
            'contextinstanceid' => $ctx->instanceid,
            'userid' => 1,
            'time' => time(),
        );
        $DB->insert_record(self::$tablename, $record);
        $record->time -= 3600 * 24 * 30;
        $DB->insert_record(self::$tablename, $record);
        $record->time -= 3600 * 24 * 30;
        $DB->insert_record(self::$tablename, $record);
        $record->time -= 3600 * 24 * 30;
        $DB->insert_record(self::$tablename, $record);
        $this->assertEquals(4, $DB->count_records(self::$tablename));

        // Remove all logs before "today".
        set_config('archivelifetime', DAYSECS, 'logstore_archivelegacy');

        $this->expectOutputString(" Deleted old log records from legacy archive store.\n");
        $clean = new \logstore_archivelegacy\task\cleanup_task();
        $clean->execute();

        $this->assertEquals(1, $DB->count_records(self::$tablename));
    }

    /**
     * Tests that the events returned by the plugin are correct.
     *
     * @global \moodle_database $DB
     * @covers \logstore_archivelegacy\log\store::get_events_select_count
     * @covers \logstore_archivelegacy\log\store::get_events_select
     * @group logstore_archivelegacy
     * @group uon
     */
    public function test_read() {
        global $DB;
        $this->resetAfterTest();
        // Create some records spread over various days; test multiple iterations in cleanup.
        $ctx = context_course::instance(1);
        $record = (object) array(
            'action' => 'unittest_executed',
            'course' => "$ctx->instanceid",
            'userid' => '1',
            'time' => time(),
            'module' => 'logstore_archivelega'
        );
        $record->id = $DB->insert_record(self::$tablename, $record);
        $record2 = clone($record);
        $record2->time -= 3600 * 24 * 30;
        $record2->id = $DB->insert_record(self::$tablename, $record2);

        // Fix up the records to contain the values as an event should return them.
        $record->timecreated = (string)$record->time;
        $record->objecttable = null;
        $record->objectid = null;
        $record->target = null;
        $record->component = 'legacy';
        $record->eventname = $record->module . '_' . $record->action;
        $record->crud = 'r';
        $record->edulevel = 0;
        $record->relateduserid = $record->userid;
        $record->contextid = $ctx->id;
        $record->contextinstanceid = $record->course;
        $record->courseid = $record->course;
        $record->contextlevel = $ctx->contextlevel;
        $record->other = array(
            'action' => $record->action,
            'course' => $record->course,
            'userid' => $record->userid,
            'time' => (string)$record->time,
            'module' => $record->module,
            'info' => '',
            'ip' => '',
            'url' => '',
            'cmid' => '0',
            'id' => $record->id,
        );
        unset($record->time);
        unset($record->course);
        unset($record->module);
        unset($record->id);

        $record2->timecreated = (string)$record2->time;
        $record2->objecttable = null;
        $record2->objectid = null;
        $record2->target = null;
        $record2->component = 'legacy';
        $record2->eventname = $record2->module . '_' . $record2->action;
        $record2->crud = 'r';
        $record2->edulevel = 0;
        $record2->relateduserid = $record2->userid;
        $record2->contextid = $ctx->id;
        $record2->contextinstanceid = $record2->course;
        $record2->courseid = $record2->course;
        $record2->contextlevel = $ctx->contextlevel;
        $record2->other = array(
            'action' => $record2->action,
            'course' => $record2->course,
            'userid' => $record2->userid,
            'time' => (string)$record2->time,
            'module' => $record2->module,
            'info' => '',
            'ip' => '',
            'url' => '',
            'cmid' => '0',
            'id' => $record2->id,
        );
        unset($record2->time);
        unset($record2->course);
        unset($record2->module);
        unset($record2->id);

        set_config('enabled_stores', 'logstore_archivelegacy', 'tool_log');
        $manager = get_log_manager(true);

        $stores = $manager->get_readers();
        $this->assertCount(1, $stores);
        $this->assertEquals(array('logstore_archivelegacy'), array_keys($stores));
        $store = $stores['logstore_archivelegacy'];
        $this->assertInstanceOf('logstore_archivelegacy\log\store', $store);
        $this->assertInstanceOf('core\log\sql_reader', $store);
        $this->assertFalse($store->is_logging());

        $this->assertTrue($store->get_events_select_exists('', []));
        $this->assertFalse($store->get_events_select_exists('userid = :userid', ['userid' => 200]));
        $this->assertSame(2, $store->get_events_select_count('', array()));
        // Is actually sorted by "timecreated ASC, id ASC".
        $events = $store->get_events_select('', array(), 'timecreated ASC', 0, 0);
        $this->assertCount(2, $events);
        $event1 = array_shift($events);
        $event2 = array_shift($events);

        $this->assertEquals((array)$record2, $event1->get_data());
        $this->assertEquals((array)$record, $event2->get_data());
    }

    /**
     * Tests that the events returned by the plugin are correct.
     *
     * @global \moodle_database $DB
     * @covers \logstore_archivelegacy\log\store::get_events_select_iterator
     * @covers \logstore_archivelegacy\log\store::get_log_event
     * @group logstore_archivelegacy
     * @group uon
     */
    public function test_read_traversable() {
        global $DB;
        $this->resetAfterTest();
        // Create some records spread over various days; test multiple iterations in cleanup.
        $ctx = context_course::instance(1);
        $record = (object) array(
            'action' => 'unittest_executed',
            'course' => "$ctx->instanceid",
            'userid' => '1',
            'time' => time(),
            'module' => 'logstore_archivelega'
        );
        $record->id = $DB->insert_record(self::$tablename, $record);
        $record2 = clone($record);
        $record2->time -= 3600 * 24 * 30;
        $record2->id = $DB->insert_record(self::$tablename, $record2);

        // Fix up the records to contain the values as an event should return them.
        $record->timecreated = (string)$record->time;
        $record->objecttable = null;
        $record->objectid = null;
        $record->target = null;
        $record->component = 'legacy';
        $record->eventname = $record->module . '_' . $record->action;
        $record->crud = 'r';
        $record->edulevel = 0;
        $record->relateduserid = $record->userid;
        $record->contextid = $ctx->id;
        $record->contextinstanceid = $record->course;
        $record->courseid = $record->course;
        $record->contextlevel = $ctx->contextlevel;
        $record->other = array(
            'action' => $record->action,
            'course' => $record->course,
            'userid' => $record->userid,
            'time' => (string)$record->time,
            'module' => $record->module,
            'info' => '',
            'ip' => '',
            'url' => '',
            'cmid' => '0',
            'id' => $record->id,
        );
        unset($record->time);
        unset($record->course);
        unset($record->module);
        unset($record->id);

        $record2->timecreated = (string)$record2->time;
        $record2->objecttable = null;
        $record2->objectid = null;
        $record2->target = null;
        $record2->component = 'legacy';
        $record2->eventname = $record2->module . '_' . $record2->action;
        $record2->crud = 'r';
        $record2->edulevel = 0;
        $record2->relateduserid = $record2->userid;
        $record2->contextid = $ctx->id;
        $record2->contextinstanceid = $record2->course;
        $record2->courseid = $record2->course;
        $record2->contextlevel = $ctx->contextlevel;
        $record2->other = array(
            'action' => $record2->action,
            'course' => $record2->course,
            'userid' => $record2->userid,
            'time' => (string)$record2->time,
            'module' => $record2->module,
            'info' => '',
            'ip' => '',
            'url' => '',
            'cmid' => '0',
            'id' => $record2->id,
        );
        unset($record2->time);
        unset($record2->course);
        unset($record2->module);
        unset($record2->id);

        set_config('enabled_stores', 'logstore_archivelegacy', 'tool_log');
        $manager = get_log_manager(true);

        $stores = $manager->get_readers();
        $this->assertCount(1, $stores);
        $this->assertEquals(array('logstore_archivelegacy'), array_keys($stores));
        $store = $stores['logstore_archivelegacy'];
        $this->assertInstanceOf('logstore_archivelegacy\log\store', $store);
        $this->assertInstanceOf('core\log\sql_reader', $store);
        $this->assertFalse($store->is_logging());

        $iterator = $store->get_events_select_iterator('', array(), '', 0, 500);
        $this->assertTrue($iterator->valid());
        $event1 = $iterator->current();
        $this->assertInstanceOf('\core\event\base', $event1);
        $this->assertEquals((array)$record, $event1->get_data());
        $iterator->next();
        $this->assertTrue($iterator->valid());
        $event2 = $iterator->current();
        $this->assertInstanceOf('\core\event\base', $event2);
        $this->assertEquals((array)$record2, $event2->get_data());
        $iterator->next();
        $this->assertFalse($iterator->valid());
        $iterator->close();
    }
}
